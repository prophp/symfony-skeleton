/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

import { createApp, h } from 'vue';
import HelloWorld from './components/HelloWorld.vue';

createApp({
    render: () => h(HelloWorld, { message: 'Hello Vue 3 with TypeScript!' })
}).mount('#app');