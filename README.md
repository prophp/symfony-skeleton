# Set up

### [SetUp.md](README%2FSetUp.md)

# How to set up same environment manually

## Sorted

### [InstallSymfony.md](README%2FInstallSymfony.md)

### [InstallVueAndTypescript.md](README%2FInstallVueAndTypescript.md)

## Unsorted

### [PublishDockerImage.md](README%2FPublishDockerImage.md)