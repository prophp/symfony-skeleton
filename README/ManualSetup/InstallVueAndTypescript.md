
---

Get ready with [InstallSymfony.md](InstallSymfony.md)

---

### Install Node.js + NPM

#### Open a new terminal

```shell
cd symfony/STACK/symfonyAndVue.v4
```

```shell
docker-compose exec server bash
```

```shell
npm install --save-dev @symfony/webpack-encore
```

Terminal output:
```
bash: npm: command not found
```

```shell
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.31.3/install.sh | bash
```

```shell
nvm install node
```

Terminal output:
```
bash: nvm: command not found
```

Restart docker-compose:

```shell
nvm install node
```

'npm' command is working now

```shell
npm -v
```

---

Now Docker-image stack is ready. It is possible to publish docker-image following these instructions:

[PublishDockerImage.md](PublishDockerImage.md)

---


### Install Symfony Encore

```shell
composer require symfony/webpack-encore-bundle
```

```shell
npm i
```

```shell
npm run dev
```

#### Open in browser

http://localhost:9999/

Background should be `lightgray` now

---

### Watch changes

```shell
npm run dev -- --watch
```

Now you can update `symfony/STACK/symfonyAndVue.v4/assets/styles/app.css` and it will be compiled automatically

---

### Install Vue

`@todo` Use stable version as well

```shell
npm install --save vue@next
```

```shell
npm install --save-dev vue-loader vue-template-compiler
```

```shell
npm install vue-class-component@next
```

Update

```
symfony/STACK/symfonyAndVue.v4/webpack.config.js
```

```shell
var Encore = require('@symfony/webpack-encore');

Encore
    // other configurations...

    .enableVueLoader(() => {}, { version: 3 })

module.exports = Encore.getWebpackConfig();
```

---

### Install TypeScript

```shell
npm install --save-dev typescript ts-loader
```

Update

```
symfony/STACK/symfonyAndVue.v4/webpack.config.js
```

```shell
var Encore = require('@symfony/webpack-encore');

Encore
    // other configurations...

    .enableTypeScriptLoader()

module.exports = Encore.getWebpackConfig();
```

---

### Create Vue & TypeScript component

Create

```
symfony/STACK/symfonyAndVue.v4/assets/components/HelloWorld.vue
```

```vue
<template>
  <div>{{ message }}</div>
</template>

<script lang="ts">
import { Vue, Options } from 'vue-class-component';

@Options({
  props: {
    message: String
  }
})
export default class HelloWorld extends Vue {
  message: string
}
</script>
```

Rename & Update

From:
```
symfony/STACK/symfonyAndVue.v4/assets/app.js
```

To:

```
symfony/STACK/symfonyAndVue.v4/assets/app.ts
```

```ts
/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

import { createApp, h } from 'vue';
import HelloWorld from './components/HelloWorld.vue';

createApp({
    render: () => h(HelloWorld, { message: 'Hello Vue 3 with TypeScript!' })
}).mount('#app');
```

Update

```
symfony/STACK/symfonyAndVue.v4/webpack.config.js
```

```ts
.addEntry('app', './assets/app.ts')
```


Create

```
symfony/STACK/symfonyAndVue.v4/tsconfig.json
```

```json
{
  "compilerOptions": {
    "target": "es5",
    "module": "ES2020",
    "moduleResolution": "node",
    "strict": true,
    "allowSyntheticDefaultImports": true,
    "esModuleInterop": true
  },
  "include": [
    "assets/**/*.ts",
    "assets/**/**/*.vue"
  ]
}
```

Create

```
symfony/STACK/symfonyAndVue.v4/assets/vue-shims.d.ts
```

```ts
declare module "*.vue" {
    import { defineComponent } from 'vue';
    const component: ReturnType<typeof defineComponent>;
    export default component;
}
```

Update

```
symfony/STACK/symfonyAndVue.v4/templates/homepage.html.twig
```

```twig
{% extends 'base.html.twig' %}

{% block title %}Homepage{% endblock %}

{% block body %}
    <div id="app"></div>
{% endblock %}
```

---

### Compile Vue components

```shell
npm run dev -- --watch
```

#### Open in browser

http://localhost:9999/


Output:

Hello Vue 3 with TypeScript!
