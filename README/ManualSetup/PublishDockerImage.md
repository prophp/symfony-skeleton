https://hub.docker.com/

anyusername
anypassword

```shell
docker ps -a
```

docker commit [CONTAINER ID] [New image name] (docker commit c9c7f075a53c infotechnohelp/apache-php7.4-mysql)

```shell
docker commit 5be612fc7e8c anyusername/any-image-title
```

echo "YourPassword" | docker login -u YourUsername --password-stdin

```shell
echo "anypassword" | docker login -u anyusername --password-stdin
```

```shell
docker image push anyusername/any-image-title
```

If fails, check that repository was created! https://hub.docker.com/repository/create?namespace=nikolajev

```shell
docker logout
```

