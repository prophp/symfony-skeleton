# Symfony project

```
GIT
Apache
PHP 8.2
Symfony CLI 5.5.6
Symfony 6.3 WebApp
Mysql @latest
TypeScript
Vue 3
```

@todo

```
VueUse
Tailwind CSS
PHP Unit
```
---

### Open initialized SANDBOX project (@todo `prophp/symfony-sandbox`)

@todo Reference to `prophp/symfony-sandbox` page

---

### Run docker

`docker/run` from the root (terminal)

```shell
docker/run
```

images: `thecodingmachine/php:8.2-v4-apache` + `mysql` (@todo Ne need for mysql in stable SANDBOX)

---

Set up Symfony CLI

```shell
symfony
```

bash: symfony: command not found

Install Symfony CLI

```
curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | sudo -E bash
```

```
sudo apt install symfony-cli
```

---

### Create Symfony project

```shell
git config --global user.email "you@example.com"

git config --global user.name "Your Name"

symfony new symfony/STACK/symfonyAndVue.v1 --version="6.3.*" --webapp
```

---

### Open in browser to see a starting page:

http://localhost:9999/symfony/STACK/symfonyAndVue.v1/public/

---

### Set up `.htaccess` files

http://localhost:9999/symfony/STACK/symfonyAndVue.v1/

```
Forbidden

You don't have permission to access this resource.
Apache/2.4.41 (Ubuntu) Server at localhost Port 9999
```

Add `/symfony/STACK/symfonyAndVue.v1/.htaccess` file

```
             <IfModule mod_rewrite.c>
            	RewriteEngine On
                 RewriteRule ^(.*)$ public/$1 [L]
             </IfModule>
```

Add `/symfony/STACK/symfonyAndVue.v1/public/.htaccess` file

```
<IfModule mod_rewrite.c>
    RewriteEngine On

    # Rewrite all requests to the index.php file
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteRule ^(.*)$ index.php [QSA,L]
</IfModule>

<IfModule !mod_rewrite.c>
    <IfModule mod_alias.c>
        RedirectMatch 302 ^/$ /index.php/
    </IfModule>
</IfModule>
```

Now you should see the Exception page

http://localhost:9999/symfony/STACK/symfonyAndVue.v1/

```
ResourceNotFoundException
NotFoundHttpException
HTTP 404 Not Found
No route found for "GET http://localhost:9999/symfony/STACK/symfonyAndVue.v1/"
```

This is totally fine as we did not set any homepage controller

---

### Create TEST Controller

`symfony/STACK/symfonyAndVue.v1/src/Controller/HomepageController.php`

```php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController
{
    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        return new Response('Hello, World!');
    }
}
```

http://localhost:9999/symfony/STACK/symfonyAndVue.v1/

We still see 'No route found' exception page. Time to move to a separate docker container for `SANDBOX/symfony/STACK/symfonyAndVue.v1`

---

### Set up docker for `symfony/STACK/symfonyAndVue.v1`

#### Run from SANDBOX root

```shell
docker/stop
```

Rewrite `symfony/STACK/symfonyAndVue.v1/docker-compose.yml` file

```yaml
version: '3'

services:

  server:
    image: thecodingmachine/php:8.2-v4-apache
    ports:
      - 9999:80
    volumes:
      - ./:/var/www/html
    links:
        - mysql

  mysql:
    image: mysql:latest
    restart: always
    command: --default-authentication-plugin=mysql_native_password
    ports:
      - 9998:3306
    environment:
      MYSQL_DATABASE: database
      MYSQL_USER: user
      MYSQL_PASSWORD: password
      MYSQL_ROOT_PASSWORD: password
    volumes:
      - ./docker-mysql:/var/lib/mysql
```

#### `NB!` Delete `symfony/STACK/symfonyAndVue.v1/docker-compose.override.yml` file


```shell
cd symfony/STACK/symfonyAndVue.v1
```

```shell
docker-compose up
```

Now `HomepageController.php` should work fine:

#### Open in browser

http://localhost:9999/

#### `NB!` There is no `Symfony Profiler` yet (debugging toolbar at the bottom)

---

### Add `docker-mysql` dir to `.gitignore` file

Project's `.gitignore` file

```
...

/docker-mysql/
```


---

### Use TWIG

Create `symfony/STACK/symfonyAndVue.v1/templates/homepage.html.twig` file

```
symfony/STACK/symfonyAndVue.v1/templates/homepage.html.twig
```

```twig
{% extends 'base.html.twig' %}

{% block title %}Homepage{% endblock %}

{% block body %}
    Hello, World! I use TWIG now
{% endblock %}
```

Rewrite `symfony/STACK/symfonyAndVue.v1/src/Controller/HomepageController.php` file

```php
namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomepageController extends AbstractController
{
    #[Route('/', name: 'homepage')]
    public function index(): Response
    {
        return $this->render("homepage.html.twig");
    }
}
```

#### `NB!` Now `Symfony Profiler` is working correctly
