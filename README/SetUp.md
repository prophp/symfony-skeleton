# Set up

```shell
composer create-project prophp/symfony-skeleton test_project
```

Got to `test_project` dir and run in terminal

```shell
docker-compose exec server bash
```

```shell
npm i
```

```shell
npm run build
```